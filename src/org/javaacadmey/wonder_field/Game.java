package org.javaacadmey.wonder_field;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {

    private final static int players = 3;
    private final static int countRound = 4;
    private final static int groupCountRound = 3;
    private final static int indexFinalRound = 3;

    public static Scanner scanner = new Scanner(System.in);

    private List<String> question = new ArrayList<>();
    private List<String> answer = new ArrayList<>();



    public void initGames() throws InterruptedException {

        System.out.println("Запуск игры \"Поле Чудес\" - подготовка к игре. Вам нужно ввести вопросы и ответы для игры.");
        for (int i = 0; i < 4 ; i++) {
            System.out.println("Введите вопрос #" + i);
            question.add(scanner.nextLine());

            System.out.println("Введите ответ вопрос #" + i);
            answer.add(scanner.nextLine());
        }

        System.out.println("Иницализация закончена, игра начнется через 5 секунд");
        Thread.sleep(5000);

        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

}
